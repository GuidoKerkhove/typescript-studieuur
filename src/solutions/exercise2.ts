import { Omit } from '../examples/omit';

/*
*   In opdracht 2 gaan we een type maken om meegegeven keys optioneel te maken.
*   Meegegeven de interface IExercise2, van deze interface gaan we een nieuwe interface maken IAnswer
*   Deze interface moet de values id, value, text, type als required hebben en question en questionNumber optioneel.
*   Om dit resultaat te bereiken maak je je eigen type waarmee je de interface gaat extenden of direct als type gebruikt. interface IAnswer extends SomeType<IExercise,...>
*   Zorg ervoor dat deze file compiled.
*   Alle examples zijn standaard te gebruiken behalve "omit" deze moet apart geimporteerd worden.
*/

interface IExercise2{
    id: string
    value: string;
    text: string;
    question:string;
    questionNumber: number;
    type: 'single_answer' | 'multiple_choice' | 'other'
}

type IAnswer<T, K extends keyof T> = Partial<T> & Omit<T, K>;

const a: IAnswer<IExercise2, 'question' | 'questionNumber'> = {
    id: '1',
    value: 'value',
    text: 'some text',
    type: 'other',
}
