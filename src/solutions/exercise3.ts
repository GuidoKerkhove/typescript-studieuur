import { Omit } from '../examples/omit';



interface IExercise3 {
    readonly code: number | string;
    readonly value: string | number | boolean | Array<any> | Function | object;
    readonly type: 'error' | 'success';
}

const item: IExercise3 = {
    code: 5,
    type: 'error',
    value: 'This, is, a, string'
}


type MakeString<T> = {
    [P in keyof T]: string;
}
type MakeNumber<T> = {
    [P in keyof T]: number;
}

type RemoveReadonly<T> = {
    -readonly [P in keyof T]: T[P];
}

type MakeKeysStringAndNumber<T, K extends keyof T, K2 extends keyof T> =
    RemoveReadonly<
        Pick<MakeString<T>, K>
        & Pick<MakeNumber<T>, K2>
    >
    & Omit<T, K | K2>

const itemValueTyped: MakeKeysStringAndNumber<IExercise3, 'value', 'code'> = {
    code: 31,
    type: item.type,
    value: 'This, is, a, string',
}

testIt(itemValueTyped);

function testIt(obj: {
    code: number;
    value: string;
    type: 'error' | 'success';
}) {
    const split = obj.value.split(', ');

    console.log(split);
}

itemValueTyped.code = 35;
itemValueTyped.value = 'hello, hello, how, are, you';

