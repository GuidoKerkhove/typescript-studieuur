




namespace Exercise4 {
    interface IExercise4 {
        value: number;
        type: number;
        code: number;
        text: string;
        title: string;
        isPayed: boolean;
        invoice: {
            number: number;
            totalPrice: number;
            code: number;
            bankAccountNumber: number;
            invoiceId: string;
            invoiceRow: IInvoiceRow[];
        }
    }

    interface IInvoiceRow {
        invoiceRowId: string;
        price: number;
        isComplete: boolean;
    }

    //make all numbers strings;

    type RecursiveNumberToString<T> = {
        [P in keyof T]: T[P] extends Array<infer U>
            ? Array<RecursiveNumberToString<U>>
            : T[P] extends number
                ? string
                : RecursiveNumberToString<T[P]>
    }

    const stringified: RecursiveNumberToString<IExercise4> = {
        code: '15:3',
        isPayed: true,
        text: 'Some text',
        title: 'This is a title',
        type: 'Type_01',
        value: '5008_13.6',
        invoice: {
            number: 'something',
            invoiceId: '1',
            bankAccountNumber: '1234567890',
            code: '5',
            totalPrice: '€12.00',
            invoiceRow:
                [
                    {
                        price: '€3.00',
                        invoiceRowId: '1',
                        isComplete: true

                    },
                    {
                        price: '€9.00',
                        invoiceRowId: '2',
                        isComplete: true

                    }
                ]

        }
    }
}
