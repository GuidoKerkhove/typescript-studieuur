
interface IExercise1{
    id: string;
}

interface IText extends IExercise1 {
    text: string;
    value?: never; //Bonus
}
interface IValue extends IExercise1 {
    value: string;
    text?: never; //Bonus
}

export function Text(props: IText | IValue) {
    if (props.text) {
        return `${props.id}: ${props.text}`;
    } else {
        return `${props.id}: ${props.value}`;
    }
}

Text({id:'', value: ''})
Text({id:'', text: ''})
Text({id:'', value: '', text: ''})