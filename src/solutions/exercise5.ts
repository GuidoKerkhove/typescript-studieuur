import { Draft, produce } from 'immer';
import { Action, combineReducers } from 'redux';
import { PatientActions } from '../exercise/exercise5/exercise5Actions';

export function createReducers<State, ActionTypes extends Action>(initialState: State, reducers: ReducersType<ActionTypes, State>) {
    return (state = initialState, action: ActionTypes) => {
        return produce(state, draft => {
            const currentReducer = reducers[action.type];
            return currentReducer ? currentReducer(draft, action) : undefined;
        });
    };
}
type ReducersType<ActionTypes extends Action, State = {}> = {
    [key in ActionTypes['type']]: (state: Draft<State>, action: Extract<ActionTypes, { type: key }>) => State | void;
};

export interface IPatients {
    [key: string]: IPatient
}
export interface IPatient {
    id: string,
    name: string;
    homeTown: string;
}

const initialState: IPatients = {};


const patientReducer = createReducers<IPatients, PatientActions>(initialState, {
    GET_PATIENT: (state, action) => {
        state[action.payload.id] = action.payload;
    },
    REMOVE_PATIENT: (state, action) => {
        delete state[action.payload.id]
    },
    CHANGE_NAME: (state,action) => {
        state[action.payload.id].name = action.payload.name;
    }
});


export const patientReducers = combineReducers({
    patients: patientReducer,
});



