
/*
* Record, zorgt dat een object volgens een map structuur werkt
*/

interface IRecord {
    id: number;
    text: string;
}

const arr: IRecord[] = [
    {id: 5, text: 'text'},
    {id: 6, text: 'text2'},
    {id: 7, text: 'text3'},
];
const map: Record<number, IRecord> = {};

for(const item of arr){
    map[item.id] = item;
}

console.log(map[5]) // {id: 5, text: 'text'}
