/*
* De union creëert een OR situatie tussen meerdere types
*/

type StringOrNumber = string | number;
type Keys = 'key1' | 'key2' | 'key3';

const sn: StringOrNumber = 5;
const sn2: StringOrNumber = 'hallo';
const sn3: StringOrNumber = false;

const key: Keys = 'key1';
const key2: Keys = 'key2';
const key3: Keys = 'key3';
const key4: Keys = 'key5';