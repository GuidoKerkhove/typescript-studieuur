import { IExample } from './interface';

/*
* Pick, pakt alleen de gevraagde properties van een interface
*/

type PickExample = Pick<IExample, 'text' | 'isExample'>;

const pickExample: PickExample = { isExample: true, text: 'Pick example' }

console.log(pickExample.text) //Pick example
pickExample.value = 'value';//Property 'value' does not exist on type 'Pick<IExample, "text" | "isExample">'.