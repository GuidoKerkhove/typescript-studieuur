/*
* Het never type betekent dat iets nooit een waarde kan hebben, ook geen null/undefined
*/

interface INeverExample {
    prop: string;
    prop2: string;
    value?: never;
}

const example: INeverExample = {
    prop: '',
    prop2: '',
}

const exampleNever: INeverExample = {
    prop: '',
    prop2: '',
    value: null
}
