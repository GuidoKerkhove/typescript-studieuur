import { IExample } from './interface';

/*
* Partial, maakt alle properties van een Interface optioneel
*/

type PartialExample = Partial<IExample>;

const partialExample: PartialExample = { isExample: true};

console.log(partialExample.isExample); //true
console.log(partialExample.text); //undefined
console.log(partialExample.value); //undefined