import { IExample } from './interface';

/*
* Exclude, pakt alle keys behalve de meegegeven.
*/

type ExcludeExample = Exclude<keyof IExample, 'isExample'>;

const a: ExcludeExample = 'text';
const b: ExcludeExample = 'value';
const c: ExcludeExample = 'isExample';//Type '"isExample"' is not assignable to type '"text" | "value"'.