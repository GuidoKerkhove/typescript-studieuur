/*
* Met infer kunnen we tegen de compiler zeggen dat die zelf uit moet vinden wat voor type het is. 
*/



type InferredArrayType<T> = T extends (infer U)[] ? U : T;


const stringType: InferredArrayType<string[]> = 'String was inferred from the array type';
const numberType: InferredArrayType<number[]> = 5;

const booleanNoArray: InferredArrayType<boolean> = true;
