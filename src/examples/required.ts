/*
* Partial, maakt alle properties van een Interface optioneel
*/

interface IOptionalExample {
    text?: string;
    value?: string;
    isExample?: boolean;
}

type RequiredExample = Required<IOptionalExample>;

const requiredExample: RequiredExample = { isExample: true, text: 'required'}; //Property 'value' is missing in type '{ isExample: true; text: string; }'.
