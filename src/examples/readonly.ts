import { IExample } from './interface';

/*
* Readonly, maakt alle properties van een Interface readonly
*/

type ReadonlyExample = Readonly<IExample>;

const readonlyExample: ReadonlyExample = { isExample: true, text: 'Readonly example', value: 'This is a readonly example' }

console.log(readonlyExample.text) //Readonly example
readonlyExample.text = ''; //Cannot assign to 'text' because it is a constant or a read-only property.
  
  
