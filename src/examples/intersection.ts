/*
* De intersection creëert een && situatie
*/

interface IA {
    value: string;
}

interface IB {
    text: string;
}

type ValueText = IA & IB;

const vt: ValueText = {
    text: '',
    value: ''
}
const vt2: ValueText = {
    text: '',
    value: '',
    random: ''
}
