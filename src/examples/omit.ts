import { IExample } from './interface';

/*
* Omit, custom type die het mogelijk maakt om alle properties behalve x te pakken van een interface
*/

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>; 

type OmitExample = Omit<IExample, 'text' | 'isExample'>;

const omitExample: OmitExample = { value: '' }

omitExample.value = 'value';
omitExample.text = 'invalid'; //Property 'text' does not exist on type 'Pick<IExample, "value">'.

