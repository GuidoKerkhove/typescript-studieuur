export interface IExample {
    text: string;
    value: string;
    isExample: boolean;
}