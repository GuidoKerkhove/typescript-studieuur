/*
* Een mapped type is een manier om zaken binnen een interface te wijzigen.
* De meeste types die hierin behandeld worden zijn gemaakt door mapped types te gebruiken.
* We gaan er hier een paar recreëren en zelf maken.
*/


namespace MappedTypes {

    type PartialExample<T> = {
        [P in keyof T]?: T[P];
    }

    type ReadonlyExample<T> = {
        readonly [P in keyof T]: T[P];
    }

    type PartialAndReadonly<T> = { //Dit kan natuurlijk ook zijn = PartialExample<ReadonlyExample<T>>
        readonly [P in keyof T]?: T[P];
    }

    type MakeString<T> = {
        [P in keyof T]: string;
    }

    //Door het - teken kan je een modifier weghalen. Voor de duidelijkheid bestaat ook het + teken om een modifier toe te voegen.
    type RemoveReadonly<T> = {
        -readonly [P in keyof T]: T[P];
    }

    //Dit zorgt er dus niet voor dat readonly weg is.
    type RemoveReadonlyIncorrect<T> = {
        [P in keyof T]: T[P];
    }

    interface IReadonly {
        readonly a: string;
        readonly b: string;
    }

    const readonlyRemoved: RemoveReadonly<IReadonly> = {
        a: 'test',
        b: 'testb'
    }

    readonlyRemoved.b = 'hallo';

    const readonlyRemovedIncorrect: RemoveReadonlyIncorrect<IReadonly> = {
        a: 'test',
        b: 'testb'
    }

    readonlyRemovedIncorrect.b = 'hallo';

}
