/*
*   In opdracht 1 gaan we een van 2 properties required maken
*   Meegegeven de interface IExercise1 en de functie Text
*   In deze functie is het of nodig om text mee te geven of value (Bonus punten -> ze mogen niet beiden meegegeven worden)
*   Zorg dat dit compiled
*/

interface IExercise1{
    id: string;
}

export function Text(props: IExercise1) {
    if (props.text) {
        return `${props.id}: ${prop.text}`;
    } else {
        return `${props.id}: ${prop.value}`;
    }
}