
/*
*   In opdracht 3 hebben we een interface IExercise3, deze interface heeft value & code getyped als een union tussen meerdere base types.
*   Nu is het zo dat we het liefst geen union hier willen, maar value & code moet generiek blijven dus we kunnen dat niet wijzigen.
*   We weten dat in ons geval de code type number is en value is type string. Alle properties zijn in de interface readonly.
*   Schrijf een type dat ervoor zorgt dat 'code' wordt omgevormd naar type number en 'value' type string. 
*   Beide values moeten gewijzigd kunnen worden, 'type' moet readonly blijven.
*/
namespace exercise3{

    interface IExercise3 {
        readonly code: number | string;
        readonly value: string | number | boolean | Array<any> | Function | object ;
        readonly type: 'error' | 'success';
    }
    
    const item: IExercise3 = {
        code: 5,
        type: 'error',
        value: 'This, is, a, string'
    }

    const itemValueTyped: IExercise3 = {
        code: 31,
        type: item.type,
        value: 'This, is, a, string',
    }
    
    testIt(itemValueTyped);

    function testIt(obj: {
        code: number;
        value: string;
        type: 'error' | 'success';
    }) {
        const split = obj.value.split(', ');

        console.log(split);
        return obj;
    }

    itemValueTyped.code = 35;
    itemValueTyped.value = 'hello, hello, how, are, you';
    itemValueTyped.type = 'error'; //Should error and say the property is readonly
}