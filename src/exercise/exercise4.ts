/*
*   In opdracht 4 hebben we een interface met objecten/arrays erin. De bedoeling van deze opgaven is dat we met een Type
*   alle properties die momenteel het type number hebben omzetten naar properties met het type string.
*   Het Type dat we schrijven moet dit recursive kunnen, voor alle geneste objecten/arrays.
*/




namespace Exercise4 {
    interface IExercise4 {
        value: number;
        type: number;
        code: number;
        text: string;
        title: string;
        isPayed: boolean;
        invoice: {
            number: number;
            totalPrice: number;
            code: number;
            bankAccountNumber: number;
            invoiceId: string;
            invoiceRow: IInvoiceRow[];
        }
    }

    interface IInvoiceRow {
        invoiceRowId: string;
        price: number;
        isComplete: boolean;
    }

    //make all numbers strings;


    const stringified: IExercise4 = {
        code: '15:3',
        isPayed: true,
        text: 'Some text',
        title: 'This is a title',
        type: 'Type_01',
        value: '5008_13.6',
        invoice: {
            number: 'something',
            invoiceId: '1',
            bankAccountNumber: '1234567890',
            code: '5',
            totalPrice: '€12.00',
            invoiceRow:
                [
                    {
                        price: '€3.00',
                        invoiceRowId: '1',
                        isComplete: true

                    },
                    {
                        price: '€9.00',
                        invoiceRowId: '2',
                        isComplete: true

                    }
                ]

        }
    }
}
