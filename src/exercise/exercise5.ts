/**
 * In opdracht 5 gaan we aan de slag met de functie createReducers. Deze functie is momenteel volledig getyped met any.
 * De bedoeling is dat we gaan zorgen dat de output van createReducers volledig getyped is.
 * Dit betekent dat op R35 de params state & action getyped zijn. 
 * Verder moet er waar createReducers wordt aangeroepen een error optreden, omdat CHANGE_NAME niet gedefined is in het object.
 * Reducers is een onderwerp binnen redux, waarbij immutable state changes optreden via actions/reducers.
 * Verder maken we binnen de reducers gebruik van immer, dit is een manier om immutable state via een mutable manier te wijzigen.
 */

// Tip, Draft is een type van immer. Deze ga je waarschijnlijk nodig hebben.
import { Draft, produce } from 'immer';
import { Action, combineReducers } from 'redux';

export function createReducers(initialState: any, reducers: any) {
    return (state = initialState, action: any) => {
        return produce(state, draft => {
            const currentReducer = reducers[action.type];
            return currentReducer ? currentReducer(draft, action) : undefined;
        });
    };
}



export interface IPatients {
    [key: string]: IPatient
}
export interface IPatient {
    id: string,
    name: string;
    homeTown: string;
}

const initialState: IPatients = {};


const patientReducer = createReducers(initialState, {
    GET_PATIENT: (state, action) => {
        state[action.payload.id] = action.payload;
    },
    REMOVE_PATIENT: (state, action) => {
        delete state[action.payload.id]
    },
});


export const patientReducers = combineReducers({
    patients: patientReducer,
});



