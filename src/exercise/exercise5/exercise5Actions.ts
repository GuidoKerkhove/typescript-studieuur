import { patients } from './patients';

export enum PatientActionsTypes {
    GET_PATIENT = 'GET_PATIENT',
    REMOVE_PATIENT = 'REMOVE_PATIENT',
    CHANGE_NAME = 'CHANGE_NAME',
}

type getPatient = {
    type: PatientActionsTypes.GET_PATIENT,
    payload: {
        id: string,
        name: string,
        homeTown: string,
    }
}

export function getPatient(id: string): getPatient{
    return { 
        type: PatientActionsTypes.GET_PATIENT,
        payload: patients[id] 
    };
}

type removePatient = {
    type: PatientActionsTypes.REMOVE_PATIENT,
    payload: {
        id: string,
    }
}

export function removePatient(id: string): removePatient {
    return {
        type: PatientActionsTypes.REMOVE_PATIENT,
        payload: {
            id
        }
    }
}

type changeName = {
    type: PatientActionsTypes.CHANGE_NAME,
    payload: {
        id: string,
        name: string
    }
}

export function changePatientName(id: string, name: string): changeName {
    return {
        type: PatientActionsTypes.CHANGE_NAME,
        payload: {
            id,
            name
        }
    }
}

export type PatientActions = getPatient | removePatient | changeName;
