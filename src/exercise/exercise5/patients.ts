import { IPatients } from '../exercise5';

export const patients: IPatients = {
    '1': {
        id: '1',
        name: 'Jan',
        homeTown: 'Amsterdam'
    },
    '2': {
        id: '2',
        name: 'Piet',
        homeTown: 'Utrecht'
    },
    '3': {
        id: '3',
        name: 'Anne',
        homeTown: 'Groningen'
    },
    '4': {
        id: '4',
        name: 'Marie',
        homeTown: 'Rotterdam'
    },
}